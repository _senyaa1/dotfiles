sudo pacman -S i3-gaps rxvt-unicode rofi vis picom firefox virtualbox code-oss
yay -s spotify


cd /config
cp -r * ~/.config/

cd ..
cp .Xresources ~/.Xresources
xrdb ~/.Xresources

cd .fonts
mkdir ~/.fonts
cp * ~/.fonts/

echo Done
